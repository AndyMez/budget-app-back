DROP TABLE IF EXISTS budget;

CREATE TABLE budget(  
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL ,
    amount FLOAT NOT NULL ,
    category VARCHAR (60),
    comment VARCHAR (255),
    date DATE NOT NULL
);

INSERT INTO budget (amount, category, comment, date) VALUES
(-8, 'Bar', 'Le Filanthrope', "2021-06-21"), 
(-9.99, 'Livres, Magazines','Abonnement Spotify', "2021-06-15"), 
(+500, 'Virement reçu', 'Rémunération CAF', "2021-06-04"), 
(-3, 'Frais bancaires', 'Frais bancaires', "2021-06-07"),
(-8.90, 'Alimentaire', 'Jacquier', "2021-07-07");
