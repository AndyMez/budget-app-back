import { Router } from "express";
import { budgetRepository } from "../repository/budget_repository";
import { addBudget, findById } from "../repository/budget_repository";

export const budgetController = Router();

// FindAll
budgetController.get('/', async (req, res) => {
    try {
        let result = await new budgetRepository().findAll();
        res.json(result)
    } catch (error) {
        res.status(500).json(error);
    }
})

// FindById
budgetController.get('/:id', async (req,res) => {
    try {
        let result = await new budgetRepository().findById(req.params.id);
        res.json(result)
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

// FindByMonth
budgetController.get('/month/:date', async(req, res)=>{
    let result = await new budgetRepository().findByDate(req.params.date);
    res.json(result)
})

// Add
// budgetController.post('/', async (req,res) => { 
//     try {
//         let toAdd = req.body
//         await new budgetRepository().addBudget(toAdd);
//         res.json(toAdd);  
//     } catch (error) {
//         console.log(error);
//         res.status(500).json(error)
//     }
// })

budgetController.post('/', async (req,res) => { 
    let id = await addBudget (req.body);
    res.status(201).json(await findById(id))
})

budgetController.get('/one/:id', async (req,resp)=>{
    let budget = await findById(req.params.id);
    if(!budget){
        resp.status(404).json ({error : 'Pas trouvé'});
    return;
    }
    resp.json(budget)
})


// Update
budgetController.patch('/', async (req, res) => {
    await new budgetRepository().update(req.body);
    res.end();
})

// Delete
budgetController.delete('/:id', async (req, res) => {
    try {
        await new budgetRepository().delete({id:req.params.id});
        res.status(204).end()
    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }
})