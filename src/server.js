import express from "express";
import { budgetController } from "./controller/budget-controller";
import cors from 'cors';
export const server = express();

server.use(express.json());

server.use(cors());
server.use('/api/budget', budgetController);
