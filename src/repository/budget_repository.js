import { Budget } from "../entity/budget";
import { createPool } from 'mysql2/promise';

export const connection = createPool(process.env.DATABASE_URL);

export class budgetRepository {

    async findAll() {
        const [rows] = await connection.execute('SELECT * FROM budget');
        const budget = [];
        for (const row of rows) {
            let instance = new Budget(row.amount, row.category, row.comment, row.date, row.id);
            budget.push(instance);
        }
        return budget 
    }

    // async findById(id) {
    //     const [rows] = await connection.execute('SELECT * FROM budget WHERE id=?', [id]);
    //     let budgetById = [];
    //     for (const row of rows) {
    //         let instance = new Budget(row.amount, row.category, row.comment, row.date, row.id)
    //         budgetById.push(instance);
    //     }
    //     return budgetById;
    // }

    async findByDate(date){
        const [rows] = await connection.execute('SELECT * FROM budget WHERE MONTH(date)=?', [date]);
        const operations = [];
        for(const row of rows){
            let instance = new Budget(row.amount, row.category, row.comment, row.date, row.id);
            operations.push(instance);
        }
        return operations;
    }

    // async addBudget(addBudget) {
    //     const [rows] = await connection.execute('INSERT INTO budget (amount, category, comment, date) VALUES (?, ?, ?, ?)', [addBudget.amount, addBudget.category, addBudget.comment, addBudget.date])
    // }

    async updateBudget(update) {
        const [rows] = await connection.execute('UPDATE budget SET amount=?, category=?, comment=?, date=? WHERE id=?', [update.amount, update.category, update.comment, update.date, update.id])
    }

    async delete(deleteBudget) {
        const [rows] = await connection.execute('DELETE FROM budget WHERE id=?', [deleteBudget.id])
    }
}

// ------------------------------------------

export async function findById(id){
    const [rows] = await connection.query ('SELECT * FROM budget WHERE id= ?',[id]);
    if (rows.length === 1){
        return new Budget(rows[0].amount,rows[0].category,rows[0].comment,rows[0].date, rows[0].id)
    }
    return null;
}


export async function addBudget(budget){
    let [data]= await connection.query ('INSERT INTO budget (amount, category, comment, date) VALUES (?, ?, ?, ?)', [budget.amount,budget.category,budget.comment, budget.date]);
    return budget.id = data.insertId
}
