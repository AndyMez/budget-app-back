export class Budget {

    id;
    amount;
    category;
    comment;
    date;

    /**
     * Informations concernant mon budget
     * @param {number} amount 
     * @param {string} category 
     * @param {string} comment 
     * @param {Date} date
     * @param {number} id 
     */
    constructor (amount, category, comment, date, id) {
        this.id = id;
        this.amount = amount;
        this.category = category;
        this.comment = comment;
        this.date = date;
    }
}