import 'dotenv-flow/config';
import { server } from './server';

let port = process.env.PORT || 8000;

server.listen(port, ()=>{
    console.log('server is running on port '+port);
})


process.on('SIGINT', () => { 
    console.log("exiting…"); 
    process.exit(0); 
});

process.on('exit', () => { 
    console.log("exiting…"); 
    process.exit(0); 
});
process.on('uncaughtException', err => {
    console.log(err, 'Uncaught Exception thrown');
    process.exit(0);
});